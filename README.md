# Windows 11 Install Unsupported Hardware 

Install Windows 11 on any potato.

# Download Rufus and Ventoy for your USB here
* https://rufus.ie/en/

* https://www.ventoy.net/en/index.html

# Download Windows 11 Release Build here:
* https://www.microsoft.com/en-us/software-download/windows11

# During installation when given the "bla bla your pc does not meet minimum requirements" press this key combination:
* Shift+F10

# In the command line that appeared like magic type:
* regedit

# Now that you are in the registry do the following find this entry:
* HKEY_LOCAL_MACHINE\SYSTEM\Setup

# Create a new key under it with a name LabConfig so it should look like:
*  HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig

# Now create 3 DWORD 32bit values after right-clicking:

* BypassRAMCheck (set value to 1)
* BypassTPMCheck (set value to 1)
* BypassSecureBootCheck (set value to 1)

# Set all these values to 1 by right-clicking and selecting Modify

Thats it,enjoy your Windows 11 on your glourious PC!
See the picture REGEDIT.JPG on how it should look like once you are done

# If something still works wonky after these steps launch regedit again and go here create/modify the following registry key:

* HKEY_CURRENT_USER\SOFTWARE\Microsoft\PCHC 

# Add DWORD 32bit value 

* UpgradeEligibility (set value to 1)

# Go to the registry key LabcConfig that we created again: 

* HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig

# Create additional DWORD 32bit values

* BypassStorageCheck 

* BypassCPUCheck 

* BypassDiskCheck 

# Finally go here and create/modify this registry key so it looks like this:

* HKEY_LOCAL_MACHINE\SYSTEM\Setup\MoSetup

# Add DWORD 32bit value:

* AllowUpgradesWithUnsupportedTPMOrCPU (set value to 1) 

# Subscribe to my youtube channel for more videos:
* https://www.youtube.com/c/SilentGameplayZZ/

# Short youtube video on how to:
* https://youtu.be/sgwbApOv0EA
